const express = require('express');
const config = require("../config");
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const auth = require('../middleware/auth');
const Photos = require('../models/Photos');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPathPhotos);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const criteria = {};
    if (req.query.user) {
      criteria.user = req.query.user;
    }
    const photos = await Photos.find(criteria).populate('user', '_id, displayName');
    res.send(photos);
  } catch(e) {
    if(e.name === 'ValidationError') {
      return res.status(400).send(e)
    }
    return res.status(500).send(e)
  }
});

router.post('/', [auth, upload.single('image')], async (req, res) => {
  const photo = new Photos({user: req.user._id, ...req.body});

  if (req.file) {
    photo.image = req.file.filename;
  }

    await photo.save()
      .then(response => Photos.populate(response, 'user').then(result => res.send(result)))
      .catch(error => res.status(400).send(error))
});

router.delete('/:id', [auth], (req, res) => {
  Photos.deleteOne({_id: req.params.id})
    .then(result => res.send(result))
    .catch(error => res.status(403).send(error))
});

module.exports = router;