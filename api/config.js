const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPathAvatar: path.join(rootPath, 'public/uploads/avatar'),
  uploadPathPhotos: path.join(rootPath, 'public/uploads/photos'),
  dbUrl: 'mongodb://localhost/finsta',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook: {
    appId: '2321508997917795',
    appSecret: 'bb40fd72d88b51ffaa7a392df9de7c10'
  }
};
