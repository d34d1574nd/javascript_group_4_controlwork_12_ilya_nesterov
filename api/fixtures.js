const mongoose = require('mongoose');
const nanoid = require('nanoid');

const config = require('./config');

const User = require('./models/User');
const Photos = require('./models/Photos');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for ( let collection of collections) {
    await collection.drop();
  }

  const users = await User.create(
    {username: 'user', displayName: 'D34D1574ND', password: '123', token: nanoid(), avatar: 'http://localhost:8000/uploads/avatar/avatar.jpeg'},
    {username: 'Ilya', displayName: 'F34R Ilya', password: '123', token: nanoid(), avatar: 'http://localhost:8000/uploads/avatar/avatar.jpeg'},
  );

  await Photos.create(
    {user: users[0]._id, title: 'Природа', image: 'pole.jpg'},
    {user: users[0]._id, title: 'Природа', image: 'priroda.jpg'},
    {user: users[1]._id, title: 'Кинотеатр', image: 'kinoteatr.jpg'},
    {user: users[1]._id, title: 'На рыбалку', image: 'ribalka.jpg'},
    {user: users[1]._id, title: 'В Греции', image: 'greece.jpg'},
    {user: users[1]._id, title: 'Еще в Греции', image: 'greece2.jpg'}
  );

  return connection.close();
};

run().catch(error => {
  console.log(error)
});