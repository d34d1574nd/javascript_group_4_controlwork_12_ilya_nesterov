const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PhotosSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  title: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  }
});

const Photo = mongoose.model('Photo', PhotosSchema);

module.exports = Photo;
