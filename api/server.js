const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const user = require('./app/users');
const photos = require('./app/photos');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
  app.use('/users', user);
  app.use('/photo', photos);

  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });
});
