import React from 'react';
import {Route, Switch} from "react-router-dom";

import Registration from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import PhotosForm from "./components/PhotosForm/PhotosForm";
import AllPhoto from "./containers/AllPhoto/AllPhoto";

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={AllPhoto}/>
      <Route path="/photo/:idUser/:username" exact component={AllPhoto}/>
      <Route path="/register" exact component={Registration}/>
      <Route path="/login" exact component={Login}/>
      <Route path="/add_photo" exact component={PhotosForm}/>
    </Switch>
  );
};

export default Routes;