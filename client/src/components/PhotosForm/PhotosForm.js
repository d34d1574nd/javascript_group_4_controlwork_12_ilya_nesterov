import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup} from "reactstrap";
import {NotificationManager} from "react-notifications";

import FormElement from "../UI/Form/FormElement";
import {postPhoto} from "../../store/actions/photoAction";

import './PhotosForm.css';


class PhotosForm extends Component {
  state = {
    title: '',
    image: ''
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.postPhoto(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    })
  };

  getFieldError = fieldName => {
    return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
  };

  render() {
    return (
      <Fragment>
        {this.props.error && this.props.error.global && (
          NotificationManager.warning(this.props.error.global)
        )}

        <Form onSubmit={this.submitFormHandler}>
          <h3 className='addPhoto'>Add new photo</h3>
          <FormElement
            value={this.state.title}
            onChange={this.inputChangeHandler}
            type="text" title='Title photos'
            propertyName='title'
            placeholder='Enter title photo'
            error={this.getFieldError('title')}
          />

          <FormElement
            propertyName="image"
            title="Your photo"
            type="file"
            onChange={this.fileChangeHandler}
            error={this.getFieldError('image')}
          />

          <FormGroup row>
            <Col sm={{offset:2, size: 10}}>
              <Button type="submit" color="primary">Save</Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  error: state.photos.photoError
});

const mapDispatchToProps = dispatch => ({
  postPhoto: PhotoData => dispatch(postPhoto(PhotoData))
});

export default connect(mapStateToProps, mapDispatchToProps)(PhotosForm);