import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavLink, UncontrolledDropdown} from "reactstrap";
import avatar from '../../../../assets/images/avatar.jpeg';
import {NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user, logout}) => (
  <UncontrolledDropdown nav inNavbar>
    <DropdownToggle nav caret>
      Hello,
      <img src={!user.avatar && user.avatar === '' ? avatar : user.avatar} className='userAvatar' alt='user'/>
      {user.role === 'admin' ? <strong>admin </strong> : null} {user.displayName}
    </DropdownToggle>
    <DropdownMenu right>
      <DropdownItem>
        <NavLink tag={RouterNavLink} to='/add_photo'>Add photo</NavLink>
      </DropdownItem>
      <DropdownItem>
        <NavLink tag={RouterNavLink} to={`/photo/${user._id}/${user.displayName}`}>My photo</NavLink>
      </DropdownItem>
      <DropdownItem divider />
      <DropdownItem onClick={logout}>
        Log out
      </DropdownItem>
    </DropdownMenu>
  </UncontrolledDropdown>
);

export default UserMenu;