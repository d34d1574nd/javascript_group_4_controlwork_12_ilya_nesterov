import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {NavLink as RouterNavLink} from 'react-router-dom';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  NavLink,
  Row
} from "reactstrap";

import {deletePhoto, getPhotos} from "../../store/actions/photoAction";
import ParamThumbnail from "../../components/ParamThumbnail/ParamThumbnail";

import './AllPhoto.css';
import {apiURL} from "../../constants";
import Spinner from "../../components/UI/Spinner/Spinner";

class AllPhoto extends Component {
  state = {
    modal: null
  };

  toggleModal = photo => {
    this.setState({modal: photo})
  };

  hideModal = () => {
    this.setState({modal: null})
  };

  componentDidMount() {
    this.props.getPhoto(this.props.match.params.idUser)
  };

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.idUser !== this.props.match.params.idUser) {
      this.props.getPhoto(this.props.match.params.idUser);
    }
  };

  render() {
    if (!this.props.photos) return <Spinner />;

    let photos = null;
    if (this.props.photos) {
      photos = this.props.photos.map(photo => {
        return (
          <Col sm={3} key={photo._id} className='photo'>
            <Card>
              <CardHeader>
                <Row>
                  <Col sm={9}>
                    <NavLink tag={RouterNavLink} to={`/photo/${photo.user._id}/${photo.user.displayName}`}>{photo.title}</NavLink>
                  </Col>
                  <Col sm={3}>
                    {this.props.user && (this.props.user._id === this.props.match.params.idUser) ?
                      <Button outline color="danger" className='buttonDelete' onClick={() => this.props.deletePhoto(photo)}>x</Button>
                      : null}
                  </Col>
                </Row>
              </CardHeader>
              <CardBody onClick={() => this.toggleModal(photo)}>
                <ParamThumbnail param="photos" image={photo.image}/>
              </CardBody>
              <CardFooter>
                Опубликовал: <NavLink tag={RouterNavLink} to={`/photo/${photo.user._id}/${photo.user.displayName}`}>{photo.user.displayName}</NavLink>
              </CardFooter>
            </Card>
          </Col>
        )
      });
    }

    return (
      <Fragment>
        <div>
          {this.props.user && this.props.match.params.idUser  ?
            <h4>{this.props.match.params.username}'s gallery</h4>
          : null}
        </div>
        <div>
          {this.props.user && (this.props.user._id === this.props.match.params.idUser) ?
            <NavLink tag={RouterNavLink} to='/add_photo'>Add photo</NavLink>
            : null}
        </div>
        <Row>
          {photos}

          <Modal size='xl' isOpen={!!this.state.modal} toggle={this.hideModal} className='modalWindow'>
            {this.state.modal &&
            <Fragment>
              <ModalHeader toggle={this.toggle}>{this.state.modal.title}</ModalHeader>
              <ModalBody>
                <img src={`${apiURL}/uploads/photos/${this.state.modal.image}`} alt="UserImage" className='imageModal'/>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onClick={this.hideModal}>Close</Button>
              </ModalFooter>
            </Fragment>
            }
          </Modal>
        </Row>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  photos: state.photos.photos,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getPhoto: userId => dispatch(getPhotos(userId)),
  deletePhoto: PhotoId => dispatch(deletePhoto(PhotoId))
});

export default connect(mapStateToProps, mapDispatchToProps)(AllPhoto);