import axios from '../../axios-api';
import {push} from 'connected-react-router';
import {NotificationManager}  from 'react-notifications';

export const FETCH_PHOTOS_SUCCESS = 'FETCH_PHOTOS_SUCCESS';
export const FETCH_PHOTOS_FAILURE = 'FETCH_PHOTOS_FAILURE';
export const LOADING_PHOTOS = 'LOADING_PHOTOS';

const fetchPhotosSuccess = photos => ({type: FETCH_PHOTOS_SUCCESS, photos});
const fetchPhotosFailure = error => ({type: FETCH_PHOTOS_FAILURE, error});
const loadingPhoto = cancel => ({type: LOADING_PHOTOS, cancel});

export const getPhotos = userId => {
  return dispatch => {
    let url = '/photo';
    if (userId) {
      url += '?user=' + userId
    }
    return axios.get(url).then(
      response => {
        dispatch(fetchPhotosSuccess(response.data));
      }
    ).finally(
      () => dispatch(loadingPhoto(false))
    );
  }
};

export const postPhoto = PhotoData => {
  return dispatch => {
    return axios.post('/photo', PhotoData).then(
      response => {
        dispatch(getPhotos(response.data.user._id));
        dispatch(push(`/photo/${response.data.user._id}/${response.data.user.displayName}`));
        NotificationManager.success('You photo loaded!');
      },
      error => {
        if (error.response) {
          dispatch(fetchPhotosFailure(error.response.data))
        } else {
          dispatch(fetchPhotosFailure({global: 'No connection'}))
        }
      }
    )
  }
};

export const deletePhoto = Photo => {
  return (dispatch) => {
    const PhotoId = Photo._id;
    return axios.delete('/photo/' + PhotoId).then(() => {
      NotificationManager.success('Photo has been deleted');
      dispatch(getPhotos(Photo.user._id));
    })
  }
};
