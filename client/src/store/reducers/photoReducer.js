import {FETCH_PHOTOS_FAILURE, FETCH_PHOTOS_SUCCESS, LOADING_PHOTOS} from "../actions/photoAction";

const initialState = {
  photoError: null,
  photos: [],
  loading: true
};

const photoReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PHOTOS_SUCCESS:
      return {...state, photos: action.photos};
    case FETCH_PHOTOS_FAILURE:
      return {...state, photoError: action.error};
    case LOADING_PHOTOS:
      return {...state, loading: action.cancel};
    default:
      return state;
  }
};

export default photoReducer;